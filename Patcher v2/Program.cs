﻿// This is an open source non-commercial project. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: https://pvs-studio.com
using System.Runtime.InteropServices;
using System.Text;
using System.Diagnostics;
using FastRsync.Delta;
using FastRsync.Diagnostics;
using Ookii.Dialogs.Wpf;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Windows.Forms;
using System.Reflection;
using System.Diagnostics.Eventing.Reader;
using System.Windows.Interop;

namespace Patcher_v2
{
	internal class Program
	{
		[DllImport( "kernel32.dll", CharSet = CharSet.Auto, SetLastError = true )]
		static extern uint GetShortPathName(
		[MarshalAs(UnmanagedType.LPTStr)]
		string lpszLongPath,
		[MarshalAs(UnmanagedType.LPTStr)]
		StringBuilder lpszShortPath,
		uint cchBuffer );

		static int Main( string[] args )
		{
			string GamePath;        // Путь к папке с игрой
			string PatchPath;       // Путь + имя файла патча
			string PatchFileName;   // Просто имя патча

			string currentVersion = Update.GetCurrentVersion();
			string info = String.Format( "Patcher v2[{0}]: Copyright (c) 2025 Krinkels [krinkels.org]\r\n", currentVersion );

			Console.WriteLine( info );

			var StrTrslt = new Dictionary<string, string>();

			// Русская локализация винды
			if( String.Equals( CultureInfo.CurrentCulture.Name, "ru-RU" ) == true )
			{
				StrTrslt.Add( "Выберете папку с игрой", "Выберете папку с игрой" );
				StrTrslt.Add( "Файл EscapeFromTarkov.exe не обнаружен по пути:\n{0}", "Файл EscapeFromTarkov.exe не обнаружен по пути:\n{0}" );
				StrTrslt.Add( "Выберете файл патча", "Выберете файл патча" );
				StrTrslt.Add( "Не удаётся определить версию EscapeFromTarkov.exe", "Не удаётся определить версию EscapeFromTarkov.exe" );
				StrTrslt.Add( "Обнаружена версии Таркова: {0}", "Обнаружена версии Таркова: {0}" );
				StrTrslt.Add( "Ищем файл патча", "Ищем файл патча" );
				StrTrslt.Add( "Ошибка парсинга имени файла патча\n{0}", "Ошибка парсинга имени файла патча\n{0}" );
				StrTrslt.Add( "Файл патча найден. Обновление из {0} в {1}", "Файл патча найден. Обновление из {0} в {1}" );
				StrTrslt.Add( "Патч не подходит для обновления.\r\nТребуется версия игры {0}, а текущая {1}", "Патч не подходит для обновления.\r\nТребуется версия игры {0}, а текущая {1}" );
				StrTrslt.Add( "Распаковываем патч", "Распаковываем патч" );
				StrTrslt.Add( "Ошибка поиска файлов патча", "Ошибка поиска файлов патча" );
				StrTrslt.Add( "Да ладно, быть не может этого, как List пустой?", "Да ладно, быть не может этого, как List пустой?" );
				StrTrslt.Add( "Будет обработано файлов: {0}", "Будет обработано файлов: {0}" );
				StrTrslt.Add( @"Файл для применения патча не найден [{0}\{2}]: {1}", @"Файл для применения патча не найден [{0}\{2}]: {1}" );
				StrTrslt.Add( @"Обновлён файл[{0}\{2}]: {1}", @"Обновлён файл[{0}\{2}]: {1}" );
				StrTrslt.Add( "UnAuthorizedAccessException: Невозможно получить доступ к файлу {0}", "UnAuthorizedAccessException: Невозможно получить доступ к файлу {0}" );
				StrTrslt.Add( " Файл только для чтения.", " Файл только для чтения." );
				StrTrslt.Add( @"Скопирован файл[{0}\{2}]: {1}", @"Скопирован файл[{0}\{2}]: {1}" );
				StrTrslt.Add( "Работа завершена", "Работа завершена" );				
				StrTrslt.Add( "Неподдерживаемый формат патча\r\nПерешлите скриншот данного сообщения автору\r\n{0}", "Неподдерживаемый формат патча\r\nПерешлите скриншот данного сообщения автору\r\n{0}" );
				StrTrslt.Add( "Произведён бэкап файла EscapeFromTarkov.exe", "Произведён бэкап файла EscapeFromTarkov.exe" );
				StrTrslt.Add( "Критическая ошибка дельты", "При попытке обновить файлы возникла критическая ошибка.\r\nДанная программа не подходит для обновления игры выбранным патчем.\r\ndelta.Apply err: {0}\r\nPatch head: {1}" );
				StrTrslt.Add( "Перешлите данное сообщение автору", "Перешлите данное сообщение автору" );
				StrTrslt.Add( "Доступна новая версия программы\n\nТекущая версия: {0}\nдоступная версия: {1}\n\nОткрыть страницу для скачивания?", "Доступна новая версия программы\n\nТекущая версия: {0}\nдоступная версия: {1}\n\nОткрыть страницу для скачивания?" );
			}
			else
			{
				StrTrslt.Add( "Выберете папку с игрой", "Select the game folder" );
				StrTrslt.Add( "Файл EscapeFromTarkov.exe не обнаружен по пути:\n{0}", "The file EscapeFromTarkov.exe was not found at the path::\n{0}" );
				StrTrslt.Add( "Выберете файл патча", "Select patch file" );
				StrTrslt.Add( "Не удаётся определить версию EscapeFromTarkov.exe", "The version of EscapeFromTarkov.exe cannot be determined" );
				StrTrslt.Add( "Обнаружена версии Таркова: {0}", "Tarkov versions detected: {0}" );
				StrTrslt.Add( "Ищем файл патча", "Looking for a patch file" );
				StrTrslt.Add( "Ошибка парсинга имени файла патча\n{0}", "Error parsing patch file name\n{0}" );
				StrTrslt.Add( "Файл патча найден. Обновление из {0} в {1}", "Patch file found. Update from {0} to {1}" );
				StrTrslt.Add( "Патч не подходит для обновления.\r\nТребуется версия игры {0}, а текущая {1}", "The patch is not suitable for updating.\r\nThe required game version is {0}, and the current one is {1}" );
				StrTrslt.Add( "Распаковываем патч", "Unpacking the patch" );
				StrTrslt.Add( "Ошибка поиска файлов патча", "Error searching for patch files" );
				StrTrslt.Add( "Да ладно, быть не может этого, как List пустой?", "Come on, this can’t be because the List is empty?" );
				StrTrslt.Add( "Будет обработано файлов: {0}", "Files to be processed: {0}" );
				StrTrslt.Add( @"Файл для применения патча не найден [{0}\{2}]: {1}", @"The file to apply the patch was not found [{0}\{2}]: {1}" );
				StrTrslt.Add( @"Обновлён файл[{0}\{2}]: {1}", @"Updated file[{0}\{2}]: {1}" );
				StrTrslt.Add( "UnAuthorizedAccessException: Невозможно получить доступ к файлу {0}", "UnAuthorizedAccessException: Unable to access file {0}" );
				StrTrslt.Add( " Файл только для чтения.", " The file is read-only." );
				StrTrslt.Add( @"Скопирован файл[{0}\{2}]: {1}", @"File copied[{0}\{2}]: {1}" );
				StrTrslt.Add( "Работа завершена", "Finished" );				
				StrTrslt.Add( "Неподдерживаемый формат патча\r\nПерешлите скриншот данного сообщения автору\r\n{0}", "Unsupported patch format\r\nSend a screenshot of this message to the author\r\n{0}" );
				StrTrslt.Add( "Произведён бэкап файла EscapeFromTarkov.exe", "A backup copy of the EscapeFromTarkov.exe file has been made" );
				StrTrslt.Add( "Критическая ошибка дельты", "A critical error occurred while trying to update files.\r\nThis program is not suitable for updating the game with the selected patch.\r\ndelta.Apply err: {0}\r\nPatch head: {1}" );
				StrTrslt.Add( "Перешлите данное сообщение автору", "Forward this message to the author" );
				StrTrslt.Add( "Доступна новая версия программы\n\nТекущая версия: {0}\nдоступная версия: {1}\n\nОткрыть страницу для скачивания?", "New version of the program is available\n\nCurrent version: {0}\nAvailable version: {1}\n\nOpen the download page?" );
			}

			if( Update.Get( StrTrslt[ "Доступна новая версия программы\n\nТекущая версия: {0}\nдоступная версия: {1}\n\nОткрыть страницу для скачивания?" ] ) == false )
				return 0;

			// Получаем путь к игре из реестра
			// Не получаем, ибо путь к игре может быть совершенно левый
			//string GamePathinREG = Registry.GetValue( @"HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\EscapeFromTarkov", "InstallLocation", null )?.ToString() ?? AppDomain.CurrentDomain.BaseDirectory;

			// Диалог выбора папки
			var dDialogSelectGameFolder = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
			dDialogSelectGameFolder.Description = StrTrslt[ "Выберете папку с игрой" ];
			dDialogSelectGameFolder.UseDescriptionForTitle = true;
			//dDialogSelectGameFolder.SelectedPath = GamePathinREG;

			if( dDialogSelectGameFolder.ShowDialog().GetValueOrDefault() == false )
			{
				// Не выбрана папка с файлом? Ну и зачем тогда продолжать?
				return 0;
			}

			// Проверим наличие EscapeFromTarkov.exe, а то вдруг не ту папку указали
			GamePath = GetShortName( dDialogSelectGameFolder.SelectedPath );
			if( System.IO.File.Exists( $"{GamePath}\\EscapeFromTarkov.exe" ) == false )
			{
				string result = String.Format( StrTrslt[ "Файл EscapeFromTarkov.exe не обнаружен по пути:\n{0}" ], dDialogSelectGameFolder.SelectedPath );
				System.Windows.Forms.MessageBox.Show( result, "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
				return 0;
			}

			// При сбое файл 123.tmp остаётся, лучше бы его удалить
			string TmpFile = $"{GamePath}\\123.tmp";

			if( System.IO.File.Exists( TmpFile ) == true )
			{
				if( DelFile( TmpFile ) == false )
					return 0;
			}

			// Диалог выбора файла
			VistaOpenFileDialog sDialogSelectFile = new VistaOpenFileDialog();
			sDialogSelectFile.Filter = "Patch file (*.update)|*.update";
			sDialogSelectFile.Title = StrTrslt[ "Выберете файл патча" ];
			// По умолчанию будем искать файл патча рядом с ехе
			sDialogSelectFile.FileName = AppDomain.CurrentDomain.BaseDirectory;

			if( sDialogSelectFile.ShowDialog().GetValueOrDefault() == false )
			{
				// Так ничего и не выбрали? Ну и зачем тогда продолжать?
				return 0;
			}

			PatchPath = sDialogSelectFile.FileName;


			//////////////////////////////////////////////////////////////
			// Проверим версию файла
			var versionInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo( GamePath + @"\EscapeFromTarkov.exe" );
			if( versionInfo.FileVersion == null )
			{
				System.Windows.Forms.MessageBox.Show( StrTrslt[ "Не удаётся определить версию EscapeFromTarkov.exe" ], "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
				return 0;
			}

			string versionexe = $"{versionInfo.FilePrivatePart}";

			Console.WriteLine( StrTrslt[ "Обнаружена версии Таркова: {0}" ], versionexe );
			Console.WriteLine( StrTrslt[ "Ищем файл патча" ] );

			// Нашли патч, узнаем из какой в какую версию патчит
			PatchFileName = System.IO.Path.GetFileName( PatchPath );
			MatchCollection matches = Regex.Matches( PatchFileName, @"\b\d{5}\b" );

			if( matches.Count != 2 )
			{
				string result = String.Format( StrTrslt[ "Ошибка парсинга имени файла патча\n{0}" ], PatchFileName );
				System.Windows.Forms.MessageBox.Show( result, "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
				return 0;
			}

			string PatchFrom = matches[ 0 ].Value;
			string PathTo = matches[ 1 ].Value;

			Console.WriteLine( StrTrslt[ "Файл патча найден. Обновление из {0} в {1}" ], PatchFrom, PathTo );

			// Сравниваем версии
			if( string.Equals( PatchFrom, versionexe ) == false )
			{
				string result = String.Format( StrTrslt[ "Патч не подходит для обновления.\r\nТребуется версия игры {0}, а текущая {1}" ], PatchFrom, versionexe );
				System.Windows.Forms.MessageBox.Show( result, "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
				return 0;
			}

			Console.WriteLine( StrTrslt[ "Распаковываем патч" ] );

			// Создадим свою папку в TEMP
			TmpFolder fTmp = new TmpFolder();
			PatchPath = GetShortName( PatchPath );

			// Распаковываем патч в папку TEMP
			try
			{
				System.IO.Compression.ZipFile.ExtractToDirectory( PatchPath, fTmp.ReturnTempPath() );
			}
			catch( DirectoryNotFoundException e )
			{
				System.Windows.Forms.MessageBox.Show( e.Message, "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
				fTmp.DeleteTmpFolder();
				return 0;   // Нет файлов и нет смысла продолжать
			}
			catch( InvalidDataException e )
			{
				System.Windows.Forms.MessageBox.Show( e.Message, "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
				fTmp.DeleteTmpFolder();
				return 0;   // Нет файлов и нет смысла продолжать
			}
						
			// Прежде чем что то делать сделаем бэкап EscapeFromTarkov.exe
			// Если пойдёт что то не так то всегда его можно восстановить
			// Ибо ежели что, то на нём часто спотыкается патчер
			System.IO.File.Copy( $"{GamePath}\\EscapeFromTarkov.exe", $"{GamePath}\\EscapeFromTarkov.exe_{versionexe}", true );
			Console.WriteLine( StrTrslt[ @"Произведён бэкап файла EscapeFromTarkov.exe" ] );

			// Найдём все файлы в распакованной папке
			List<string> mPatchList = new List<string>();
			if( FindFiles( fTmp.ReturnTempPath(), ref mPatchList ) == false )
			{
				System.Windows.Forms.MessageBox.Show( StrTrslt[ "Ошибка поиска файлов патча" ], "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
				fTmp.DeleteTmpFolder();
				return 0;
			}

			if( mPatchList.Count == 0 )
			{
				System.Windows.Forms.MessageBox.Show( StrTrslt[ "Да ладно, быть не может этого, как List пустой?" ], "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
				fTmp.DeleteTmpFolder();
				return 0;
			}

			string[] ACfiles;

			try
			{
				ACfiles = System.IO.Directory.GetFiles( $"{GamePath}\\EscapeFromTarkov_Data\\Managed", "Assembly-CSharp*" );

			}
			catch( Exception ex )
			{
				Console.WriteLine( $"Произошла ошибка: {ex.Message}" );
				return 0;
			}

			// Предполагаем что чистая установка
			if( ACfiles.Length == 2 )
			{
				Console.WriteLine( "Чистый клиент" );
			}
			else 
			if( ACfiles.Length == 3 )
			{
				string? backupFile = ACfiles.FirstOrDefault( file =>
					Path.GetFileName( file ) == "Assembly-CSharp.dll.spt-bak" ||
					Path.GetFileName( file ) == "Assembly-CSharp.dll.bak" );

				if( backupFile != null )
				{
					try
					{
						System.IO.File.Delete( $"{GamePath}\\EscapeFromTarkov_Data\\Managed\\Assembly-CSharp.dll" );
						System.IO.File.Move( backupFile, $"{GamePath}\\EscapeFromTarkov_Data\\Managed\\Assembly-CSharp.dll" );
					}
					catch( DirectoryNotFoundException e )
					{
						System.Windows.Forms.MessageBox.Show( e.Message, "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
						fTmp.DeleteTmpFolder();
						return 0;   // Если не можем переименовать файл, то и пропатчить не сможем, нет смысла продолжать
					}
					catch( UnauthorizedAccessException e )
					{
						System.Windows.Forms.MessageBox.Show( e.Message, "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
						fTmp.DeleteTmpFolder();
						return 0;   // Файл используется? Или файл недоступен? Как так то?
					}
				}
				else
				{
					var fileNames = ACfiles.Select( file => Path.GetFileName( file ) ); // Извлекаем имена файлов
					var result = string.Join( Environment.NewLine, fileNames );

					System.Windows.Forms.MessageBox.Show( $"Бэкап Assembly-CSharp не найден, дальнейшая работа невозможна.\nНайдены файлы:\n{result}", "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
					return 0;
				}
			}
			else 
			if( ACfiles.Length > 3 )
			{
				var fileNames = ACfiles.Select( file => Path.GetFileName( file ) ); // Извлекаем имена файлов
				var result = string.Join( Environment.NewLine, fileNames );

				System.Windows.Forms.MessageBox.Show( $"Файлов Assembly-CSharp* больше трёх, дальнейшая работа невозможна.\nНайдены файлы:\n{result}", "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
				return 0;
			}




			///////////////////////////////////////////////////////////////////////
			int PathLen = fTmp.ReturnTempPath().Length;


			var delta = new DeltaApplier
			{
				SkipHashCheck = true
			};
			IProgress<ProgressReport> progressReport = new ConsoleProgressReporter();

			string fName;
			string GameFile = "";
			string GameFileNew;

			int i = 1;

			Console.WriteLine( StrTrslt[ "Будет обработано файлов: {0}" ], mPatchList.Count() );

			foreach( string Str in mPatchList )
			{
				//fName = System.IO.Path.GetFileNameWithoutExtension( Str );
				fName = System.IO.Path.GetExtension( Str );

				if( string.Equals( System.IO.Path.GetExtension( fName ), ".patch" ) == true )
				{
					// Относительный путь к файлу
					fName = Str.Remove( 0, PathLen );

					fName = fName.Replace( ".patch", "" );

					GameFile = $"{GamePath}\\{fName}";
					GameFileNew = $"{GamePath}\\123.tmp";

					if( System.IO.File.Exists( GameFile ) == false )
					{
						Console.WriteLine( StrTrslt[ @"Файл для применения патча не найден [{0}\{2}]: {1}" ], i++, fName, mPatchList.Count() );
						continue;
					}

					try
					{
						System.IO.File.Move( GameFile, GameFileNew );
					}
					catch( DirectoryNotFoundException e )
					{
						System.Windows.Forms.MessageBox.Show( e.Message, "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
						return 0;   // Если не можем переименовать файл то и пропатчить не сможем, нет смысла продолжать
					}
					catch( UnauthorizedAccessException e )
					{
						System.Windows.Forms.MessageBox.Show( e.Message, "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
						fTmp.DeleteTmpFolder();
						return 0;   // Файл используется? Или файл недоступен? Как так то?
					}

					// Применяем патч на файле
					using( var basisStream = new FileStream( GameFileNew, FileMode.Open, FileAccess.Read, FileShare.Read ) )
					using( var deltaStream = new FileStream( Str, FileMode.Open, FileAccess.Read, FileShare.Read ) )
					using( var newFileStream = new FileStream( GameFile, FileMode.Create, FileAccess.ReadWrite, FileShare.Read ) )
					{
						try
						{
							delta.Apply( basisStream, new BinaryDeltaReader( deltaStream, progressReport ), newFileStream );
						}
						catch( Exception ex )
						{
							// Упс, а формат патча то не тот что нужен
							// Для начала удалим ненужные файлы		
							basisStream.Close();
							deltaStream.Close();
							newFileStream.Close();
							
							if( DelFile( GameFileNew ) == false )	// .tmp файл
								return 0;
							
							if( DelFile( GameFile ) == false )		// "Новый" файл
								return 0;
							
							// ВОсстановим EscapeFromTarkov.exe из бэкапа
							try
							{							
								System.IO.File.Move( $"{GamePath}\\EscapeFromTarkov.exe_{versionexe}", $"{GamePath}\\EscapeFromTarkov.exe" );
							}
							catch( DirectoryNotFoundException e )
							{
								System.Windows.Forms.MessageBox.Show( e.Message, "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
								fTmp.DeleteTmpFolder();
								return 0;
							}
							catch( UnauthorizedAccessException e )
							{
								System.Windows.Forms.MessageBox.Show( e.Message, "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
								fTmp.DeleteTmpFolder();
								return 0;
							}
							
							///////////////////////////////////////////////////////////////////////////////////////
							FileStream fileStream = new FileStream( Str, FileMode.Open, FileAccess.Read );
							
							byte[] buffer = new byte[ 9 ];
							int bytesRead = fileStream.Read(buffer, 0, buffer.Length);
							string HeadPatch = System.Text.Encoding.UTF8.GetString( buffer );
								
							string result = String.Format( StrTrslt[ "Критическая ошибка дельты" ], ex.Message, HeadPatch );
							System.Windows.Forms.MessageBox.Show( result, StrTrslt[ "Перешлите данное сообщение автору" ], MessageBoxButtons.OK, MessageBoxIcon.None );
						
							return 0;
						}
					}					
					
					if( DelFile( GameFileNew ) == false )
						return 0;
					
					Console.WriteLine( StrTrslt[ @"Обновлён файл[{0}\{2}]: {1}" ], i++, fName, mPatchList.Count() );
				}
				else
				{
					try
					{
						// Относительный путь к файлу
						fName = Str.Remove( 0, PathLen );
					}
					catch( ArgumentOutOfRangeException e )
					{
						System.Windows.Forms.MessageBox.Show( e.Message, "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
						return 0;
					}

					// Копируем файл с заменой
					try
					{
						GameFile = $"{GamePath}\\{fName}";

						// Проверяем, существует ли такая папка
						if( System.IO.Directory.Exists( GameFile ) == false )
						{
							// Не существует, создаём							
							System.IO.Directory.CreateDirectory( System.IO.Path.GetDirectoryName( GameFile ) );							
						}

						// А теперь пробуем копировать
						System.IO.File.Copy( Str, GameFile, true );
					}
					catch( DirectoryNotFoundException e )
					{
						System.Windows.Forms.MessageBox.Show( e.Message, "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
						return 0;
					}
					catch( UnauthorizedAccessException )
					{
						FileAttributes attr = ( new FileInfo( GameFile ) ).Attributes;

						string result;
						if( ( attr & FileAttributes.ReadOnly ) > 0 )
							result = String.Format( StrTrslt[ "UnAuthorizedAccessException: Невозможно получить доступ к файлу {0}. Файл только для чтения" ], GameFile );
						else
							result = String.Format( StrTrslt[ "UnAuthorizedAccessException: Невозможно получить доступ к файлу {0}" ], GameFile );

						System.Windows.Forms.MessageBox.Show( result, "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );

						return 0;
					}
					Console.WriteLine( StrTrslt[ @"Скопирован файл[{0}\{2}]: {1}" ], i++, fName, mPatchList.Count() );
				}
			}

			Console.WriteLine( StrTrslt[ "Работа завершена" ] );

			fTmp.DeleteTmpFolder();
			return 0;
		}

		private static string GetShortName( string sLongFileName )
		{
			var buffer = new StringBuilder( 259 );
			uint len = GetShortPathName( sLongFileName, buffer, ( uint )buffer.Capacity );
			if( len == 0 )
				throw new System.ComponentModel.Win32Exception();
			return buffer.ToString();
		}
		
		private static bool DelFile( string sFile )
		{
			try
			{
				System.IO.File.Delete( sFile );				
			}
			catch( DirectoryNotFoundException e )
			{
				System.Windows.Forms.MessageBox.Show( e.Message, "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
				return false;
			}
			catch( UnauthorizedAccessException e )
			{
				System.Windows.Forms.MessageBox.Show( e.Message, "!!!", MessageBoxButtons.OK, MessageBoxIcon.None );
				return false;
			}
			return true;
		}

		private static bool FindFiles( string path, ref List<string> arrayfiles )
		{
			try
			{
				arrayfiles.AddRange( System.IO.Directory.GetFiles( path, "*", SearchOption.AllDirectories ) );
			}
			catch( Exception )
			{
				return false;
			}
			return true;
		}
	}

	class TmpFolder
	{
		public string mTempFolder;
		public TmpFolder()
		{
			long pid = Process.GetCurrentProcess().Id ^ 0xa1234568;

			mTempFolder = $"{Path.GetTempPath()}btl_temp_{pid}";

			Directory.CreateDirectory( mTempFolder );
		}

		public void DeleteTmpFolder()
		{
			Directory.Delete( mTempFolder, true );
		}

		public string ReturnTempPath()
		{
			return mTempFolder;
		}
	}
}
