﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/// <summary>
/// Класс для проверки обновлений
/// </summary>
public static class Update
{
	/// <summary>
	/// Проверка обновлений
	/// </summary>
	/// <param name="Msg">Текст для MessageBox</param>
	/// <returns>true - всё хорошо, false - последующий выход из программы</returns>
	public static bool Get( string Msg )
	{
		string currentVersion = GetCurrentVersion();
		string gitlabApiUrl = "https://gitlab.com/api/v4/projects/55707273/releases";
		string latestVersion = GetLatestVersionFromGitLab( gitlabApiUrl );

		if( IsNewVersionAvailable( currentVersion, latestVersion ) )
		{
			string result = String.Format( Msg, currentVersion, latestVersion );
			//Console.WriteLine( $"Доступна новая версия программы: {latestVersion}" );
			if( System.Windows.Forms.MessageBox.Show( result, "!!!", MessageBoxButtons.YesNo, MessageBoxIcon.None ) == DialogResult.Yes )
			{
				OpenUrl( "https://gitlab.com/Krinkels/patcher-v2/-/releases" );
				return false;
			}
			else
				return true;
		}

		return true;
	}

	public static string GetCurrentVersion()
	{
		var versionAttribute = Attribute.GetCustomAttribute(
							Assembly.GetExecutingAssembly(),
							typeof( AssemblyInformationalVersionAttribute )
						) as AssemblyInformationalVersionAttribute;

		return versionAttribute?.InformationalVersion ?? "1.0.0.0";
	}

	private static string GetLatestVersionFromGitLab( string apiUrl )
	{
		using( HttpClient client = new HttpClient() )
		{
			HttpResponseMessage response = client.GetAsync( apiUrl ).Result;
			response.EnsureSuccessStatusCode();
			string responseBody = response.Content.ReadAsStringAsync().Result;

			if( string.IsNullOrEmpty( responseBody ) == true )
				return string.Empty;

			JArray jsonResponse = JArray.Parse( responseBody );
			return jsonResponse?[ 0 ]?[ "tag_name" ]?.ToString() ?? string.Empty;
		}
	}

	private static bool IsNewVersionAvailable( string currentVersion, string latestVersion )
	{
		Version current = new Version( currentVersion );
		Version latest = new Version( latestVersion );
		return latest > current;
	}

	private static void OpenUrl( string url )
	{
		try
		{
			Process.Start( new ProcessStartInfo
			{
				FileName = url,
				UseShellExecute = true
			} );
		}
		catch( Exception ex )
		{
			MessageBox.Show( $"Не удалось открыть страницу: {ex.Message}", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error );
		}
	}
}
