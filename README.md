## Patcher v2

[![Latest Release](https://gitlab.com/Krinkels/patcher-v2/-/badges/release.svg)](https://gitlab.com/Krinkels/patcher-v2/-/releases) 

Вторая версия патчера файлов игры EFT, написанная на C#

Внимание, пользоваться на свой трах и риск!!!!

## Запуск
Для запуска понадобятся:
[Microsoft Visual C++ 2015-2019 Redistributable](https://aka.ms/vs/16/release/vc_redist.x64.exe)
[.NET 8.0 Desktop Runtime](https://dotnet.microsoft.com/en-us/download/dotnet/thank-you/runtime-desktop-8.0.2-windows-x64-installer?cid=getdotnetcore)

а так же KBшка, если вдруг не установлена
[Security Update for Windows 7 for x64-based Systems (KB3063858)](https://www.microsoft.com/download/details.aspx?id=47442)


## Использование
1. В папку с приложением закидываем файл патча, программа сама проверит совместимость. Если вдруг патч не будет найден, то появится окошко для его выбора
2. При запуске программы появится окошко выбора папки, выбираем папку с игрой
3. Далее пойдёт процесс обновлений. Если не вылезло никаких ошибок то можно пробовать играть